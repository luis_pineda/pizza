<!DOCTYPE html>
<html>
<head>
	<title>Pizza</title>
	<link rel="stylesheet" type="text/css" href="src/css/estilo.css">
</head>
<body>
	<section>
		<div class="opciones" id="op1" onclick="pintar('yellow')"><br><b>Hawaiana</b></div>
		<div class="opciones" id="op2" onclick="pintar('green')"><br><b>Vegetariana</b></div>
		<div class="opciones" id="op3" onclick="pintar('orange')"><br><b>De don Cangrejo</b></div>
		<div class="opciones" id="op4" onclick="pintar('red')"><br><b>Peperoni</b></div>
	</section>
	<aside>
		<h1>Pizzas</h1>
		 <ul class="circle"> 
		 	<li> 
		 		<div class="relleno" id="r1" onclick=clickaction(this)></div> 
		 	</li> 
		 	<li> 
		 		<div class="relleno" id="r2" onclick=clickaction(this)></div> 
		 	</li> 
		 	<li> 
		 		<div class="relleno" id="r3" onclick=clickaction(this)></div> 
		 	</li> 
		 	<li> 
		 		<div class="relleno" id="r4" onclick=clickaction(this)></div> 
		 	</li> 
		 	<li>
		 		<div class="relleno" id="r5" onclick=clickaction(this)></div> 
		 	</li> 
		 	<li> 
		 		<div class="relleno" id="r6" onclick=clickaction(this)></div> 
		 	</li> 
		 	<li> 
		 		<div class="relleno" id="r7" onclick=clickaction(this)></div> 
		 	</li> 
		 	<li> 
		 		<div class="relleno" id="r8" onclick=clickaction(this)></div> 
		 	</li> 
		 </ul> 
	</aside>
	<script type="text/javascript" src="src/js/p.js"></script>
</body>
</html>